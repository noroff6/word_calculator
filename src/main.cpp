#include <iostream>
#include <vector>
#include <string>
#include <ctype.h>
#include <stdio.h>
#include <bits/stdc++.h>
#include "signs.h"

int main()
{

    std::string s;
    std::cout << "Please enter a number between 1 and 9: " << std::endl;

    // Get full line with whitespaces also
    std::getline(std::cin, s);
    signs(s);

    return 0;
}
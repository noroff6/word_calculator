#include "signs.h"
#include <bits/stdc++.h>


int get_digits(std::string word)
{
    std::vector<std::string> numbers_1_to_9 = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    std::vector<std::string> numbers_10_to_90 = {"ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    std::vector<std::string> numbers_100_to_1000 = {"hundred", "thousand"};
    std::vector<int> all_digits = {};
    std::vector<int> hundred_vect = {};
    std::vector<int> count_vect = {};
    std::vector<std::string> hundred_s = {};

    int singular = 0;
    int tens = 0;
    int hundred = 0;
    //int thousand = 0;
    //int count = 0;

    // Hundred not working at the moment
    if (word == "hundred")
    {
        hundred_s.push_back("hundred");
        //count++;
        for (size_t i = 0; i < 11; i++)
        {
            singular = i + 1;
            hundred = singular * 100;
            all_digits.push_back(hundred);
            all_digits.erase(std::remove(all_digits.begin(), all_digits.end(), singular), all_digits.end());
            break;
        }
    }
    else
    {   
        for (size_t i = 0; i < 11; i++)
        {
            if (word == numbers_1_to_9[i])
            {
                singular = i + 1;
                //hundred = 100 * singular;
                //int test = hundred - singular;
                //all_digits.push_back(test);
                all_digits.push_back(singular);
                //hundred_vect.push_back(hundred);
            }

            else if (word == numbers_10_to_90[i])
            {
                tens = (i + 1) * 10;
                all_digits.push_back(tens);
                break;
            }

        }
    }

    //std::cout << "Count " << count << std::endl;
    //int size_hund = hundred_s.size()+1;

    // Return sum
    int sum = std::accumulate(all_digits.cbegin() + 1, all_digits.cend(), all_digits[0], std::plus<>());
    return sum;
}
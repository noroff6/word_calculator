#include "signs.h"
#include "get_sign.h"
#include <algorithm>

std::vector<int> signs(std::string s)
{
    std::vector<std::string> numbers_1_to_9 = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    std::vector<std::string> numbers_10_to_90 = {"ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    std::vector<std::string> numbers_100_to_1000 = {"hundred", "thousand"};
    std::vector<std::string> user_input = {};
    std::vector<int> left = {};
    std::vector<int> right = {};
    std::vector<int> test = {};
    std::vector<int> digits = {};

    // int singular = 0;
    // int tens = 0;
    // int hundred = 0;
    // int thousand = 0;

    // Save mathematical signs
    std::string get_string_sign;

    std::istringstream ss(s);

    // Whole string
    std::string word;

    while (ss >> word)
    {
        // Add string to vector
        user_input.push_back(word);

        // Check sign
        if (word == "plus")
        {
            get_string_sign = "plus";
        }
        else if (word == "minus")
        {
            get_string_sign = "minus";
        }
        else if (word == "times")
        {
            get_string_sign = "times";
        }
        else if (word == "over")
        {
            get_string_sign = "over";
        }
    }

    int position = 0;
    // Iterate over input and find sign
    std::vector<std::string>::iterator it;
    it = std::find(user_input.begin(), user_input.end(), get_string_sign);

    // Save position for sign
    if (it != user_input.end())
    {
        position = it - user_input.begin();
    }
    // std::cout << "Position " << position << std::endl;

    int sum = 0;
    // Left side to sum according to sign position
    for (int i = 0; i < position; i++)
    {
        sum = get_digits(user_input[i]);
        left.push_back(sum);
    }

    // Right side to sum, according to sign position
    for (int i = (position + 1); i < user_input.size(); i++)
    {
        sum = get_digits(user_input[i]);
        right.push_back(sum);
    }

    // Sum left side
    float left_sum = std::accumulate(left.cbegin() + 1, left.cend(), left[0], std::plus<>());
    // Sum right side
    float right_sum = std::accumulate(right.cbegin() + 1, right.cend(), right[0], std::plus<>());


    // Print the answer
    if (get_string_sign == "over")
    {
        std::cout << (left_sum / right_sum) << std::endl;
    }
    if (get_string_sign == "minus")
    {
        std::cout <<  (left_sum - right_sum) << std::endl;
    }
    if (get_string_sign == "times")
    {
        std::cout <<  (left_sum * right_sum) << std::endl;
    }
    if (get_string_sign == "plus")
    {
        std::cout << (left_sum + right_sum) << std::endl;
    }

    return test;
}
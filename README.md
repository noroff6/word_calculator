# Word calculator / C++

Description
This project is an assignment from Noroff, as part of Experis Academy. 


# Table of Contents

Background
Install
Usage
Contributing
References
License


# Background
"Create an application that takes user input of a sentence.
The sentence must take a mathematical statement in words and calculate the result (2 marks)
Example:
input: "six times seven"
result: 42
input: "eight divided by three"
result: 2.6 (or 2 remainder 2)"



# Install
Clone the repo.
Install CMake:

```
$ sudo apt install cmake
```


# Usage

./main

Enter a number between 1 and 90 in letters.
Enter a calculation of number in letters, between the numbers 1 and 90.

Example:
```
fifty nine plus five
```
The following operators can be used:
- plus
- minus
- times
- over

### Constraints
Only numbers between 1 and 90 can be used.



# Contributing
This project does not use any contributors, but feel free to use the code and optimize it in your own unique way.


# License
MIT